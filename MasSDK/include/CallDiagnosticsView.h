
#import <UIKit/UIKit.h>
#import "MasStateDelegate.h"
#import "MasQualityDelegate.h"

/// @brief A view that shows diagnostics information for a VoIP call over the ACE+ SDK.
///
/// This can be created dynamically using init:, initWithFrame:, etc...
/// Or it can be set as the custom class for a UIView in your interface builder file .
@interface CallDiagnosticsView: UIView<MasStateDelegate, MasQualityDelegate>

/// @brief A flag that can be set to show extra space at the top for a transparent status bar.
///
/// This flag is useful when showing the view dynamically as a fullscreen dialog, since the
/// status bar will be overlayed on top of the view.
///
/// This should not be set if the view is not fullscreen, or if the status bar is not transparent,
/// i.e. when the status bar is green / red because there is an ongoing call.
@property ( nonatomic ) BOOL hasSpaceForStatusBar;

/// @brief A flag that can be set to show a small 'x' close button at the top right of the view.
///
/// When the close button is pressed, closeView is called.
/// If this is false, it is up to the caller to manage the lifecycle of this view.
@property ( nonatomic ) BOOL hasCloseButton;

/// @brief Close the call diagnostics view.
///
/// The default implementation just calls removeFromSuperview.
/// This can be overridden as needed.
- ( void ) closeView;

@end
