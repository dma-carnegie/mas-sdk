
#import "MasSeamlessEvent.h"
#import "MasSchedulerType.h"
#import "Interface.h"

/// @brief Contains the state of the MAS seamless scheduler.
@interface MasSeamlessState: NSObject

/// @brief The last event that caused the seamless scheduler state to change.
@property ( readonly, nonatomic ) MasSeamlessEvent lastEvent;

/// @brief The scheduler that this event is for.
@property ( readonly, nonatomic ) MasSchedulerType scheduler;

/// @brief The physical interface that is selected by the seamless scheduler to pass traffic.
///
/// @note This value is nil when isReplicating is true.
@property ( readonly, nonatomic, strong ) Interface * selectedInterface;

/// @brief Whether the seamless scheduler is replicating across all links to find the best link.
///
/// @note If replicating, then the value of selectedInterface is nil.
@property ( readonly, nonatomic ) BOOL isReplicating;

@end
