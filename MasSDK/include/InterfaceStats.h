
#import "Interface.h"

/// @brief Contains statistics for traffic over an interface.
@interface InterfaceStats: NSObject<NSCoding>

/// @brief The physical interface for these stats.
@property ( readonly, nonatomic, strong ) Interface * interface;

/// @brief Number of bytes sent over the interface (since the beginning).
@property ( readonly, nonatomic ) uint64_t bytesSent;

/// @brief Number of bytes received over the interface (since the beginning).
@property ( readonly, nonatomic ) uint64_t bytesReceived;

@end
