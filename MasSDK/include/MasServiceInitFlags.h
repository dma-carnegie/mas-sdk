
// This header contains flags that can be used when initializing MasService for the first time.
// These flags cannot be changed after MasService is started and stopped, except by restarting the application.
// These flags can be ORed together as a bitmask.

/// @brief Pass this flag to disable the use of the virtual sockets.
/// When passed, MAS virtual sockets will behave like regular socket functions.
/// NOTE: This flag takes priority over other flags.
extern const int MasServiceInitFlagDisabled;

/// @brief Pass this flag to disable falling back to regular sockets, when we can't communicate with MasService.
/// NOTE: Fallback will always be used regardless of this flag if MasServiceInitFlagDisabled is passed.
extern const int MasServiceInitFlagDisableNativeFallback;

/// @brief Pass this flag to disable debug logs.
extern const int MasServiceInitFlagDisableLogs;
