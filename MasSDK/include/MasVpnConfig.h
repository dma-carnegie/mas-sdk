
#import <Foundation/Foundation.h>

/// @brief Represents configuration for MasService's VPN mode.
/// The default configuration of the VPN only allows enabling/disabling manually via enableVpn/disableVpn.
///
/// Anything in this configuration only applies if enableVpnWithConfig: returns MasVpnServiceStatusCodeSuccess.
/// Furthermore, if VPN permission are revoked after enabling, then the configuration won't be applied
/// until VPN permission is granted again.
@interface MasVpnConfig: NSObject<NSCoding>

/// @brief Whether the VPN will only be enabled when connected to Wi-Fi.
/// The allowedSsids can optionally be set to only allow specific Wi-Fi networks.
@property ( nonatomic ) BOOL enableOnlyOnWiFi;

/// @brief The set of Wi-Fi SSIDs that MasService will enable the VPN on when enableOnlyOnWiFi=YES.
/// This can optionally be set to only allow specific Wi-Fi networks.
/// If this is null or empty, MasService allows all Wi-Fi networks.
@property ( nonatomic, strong ) NSSet<NSString *> * allowedSsids;

/// @brief Create a new MasVpnConfig initialized with default values for all optional fields.
/// @return A new MasVpnConfig instance.
+ ( MasVpnConfig * ) config;

@end
