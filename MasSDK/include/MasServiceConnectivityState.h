
#import <Foundation/Foundation.h>

/// @brief An enum of possible states of MasService tunnel connectivity.
typedef NS_ENUM ( uint8_t, MasServiceConnectivityState )
{
    /// @brief MasService is not connected to the server.
    ///        Sockets will not have network connectivity.
    MasServiceConnectivityStateNotConnected = 0,

    /// @brief MasService is connected to the server.
    ///        Sockets will have network connectivity through the tunnel.
    MasServiceConnectivityStateConnected,

    /// @brief MasService tunnel is disabled.
    ///        Sockets will behave as regular sockets. They will not go over the tunnel.
    MasServiceConnectivityStateDisabled
};
