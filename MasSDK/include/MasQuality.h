
#import "MasQualityDelegate.h"

/// @brief The interface that exposes MasService's connection quality via properties and delegates.
@interface MasQuality: NSObject

/// @brief The current state of the seamless scheduler.
/// This is nil if we don't have any information about it yet.
@property ( atomic, strong, readonly ) MasSeamlessState * masSeamlessState;

/// @brief Map of Interface to the link stats of the MAS connection over that interface.
@property ( atomic, strong, readonly ) NSDictionary<Interface *, LinkStats *> * linkStats;

/// @brief Map of Interface to the traffic stats of the MAS connection over that interface.
@property ( atomic, strong, readonly ) NSDictionary<Interface *, InterfaceStats *> * interfaceStats;

/// @brief Contains stats about an active VOIP call.
/// This is nil if we don't have any information about it, or there is no active call.
@property ( atomic, strong, readonly ) VoipStats * voipStats;

/// @brief Register a MasQuality delegate. This does nothing if the delegate is already registered.
/// @param [in] delegate The delegate to register.
- ( void ) registerDelegate:( id<MasQualityDelegate> )delegate;

/// @brief Unregister a MasQuality delegate. This does nothing if no such delegate is registered.
/// @param [in] delegate The delegate to unregister.
- ( void ) unregisterDelegate:( id<MasQualityDelegate> )delegate;

@end
