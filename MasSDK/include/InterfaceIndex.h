
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/// @brief Represents the indexes for interfaces.
/// This is signed so that we can use -1 to represent an unknown interface.
typedef int16_t InterfaceIndex;

/// @brief Index of the Wi-Fi interface
extern const InterfaceIndex InterfaceIndexWiFi;

/// @brief Index of the Mobile interface
extern const InterfaceIndex InterfaceIndexMobile;

#ifdef __cplusplus
}
#endif
