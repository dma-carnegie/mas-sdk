
#import <Foundation/Foundation.h>

/// @brief The different ways to match the criteria in a QualityCriteria object.
typedef NS_ENUM ( uint8_t, QualityCriteriaMatchType )
{
    QualityCriteriaMatchTypeInvalid,
    QualityCriteriaMatchTypeAll,
    QualityCriteriaMatchTypeAny
};

/// @brief Contains quality criteria that can be used to inspect and match network quality data.
@interface QualityCriteria: NSObject<NSCoding>

/// @brief How to match the criteria contained in this object.
@property ( nonatomic ) QualityCriteriaMatchType matchType;

/// @brief The round trip latency in milliseconds; or negative if any latency is permissible.
@property ( nonatomic ) int32_t latency;

/// @brief The jitter in milliseconds (std. dev. of round trip times); or negative if any jitter is permissible.
@property ( nonatomic ) int32_t jitter;

/// @brief The packet loss as a percentage; or negative if any packet loss is permissible.
///        Only values from 0-100 are valid packet loss values.
@property ( nonatomic ) int8_t packetLoss;

/// @brief The Mean Opinion Score for web traffic; or negative if any MOS is permissible.
///        Only values from 0.0-5.0 are valid MOS values.
@property ( nonatomic ) float mosWeb;

/// @brief The Mean Opinion Score for VOIP traffic; or negative if any MOS is permissible.
///        Only values from 0.0-5.0 are valid MOS values.
@property ( nonatomic ) float mosVoip;

/// @brief The Mean Opinion Score for video traffic; or negative if any MOS is permissible.
///        Only values from 0.0-5.0 are valid MOS values.
@property ( nonatomic ) float mosVideo;

/// @brief The QNS value (0-100). -1 if unknown.
///        Only values from 0-100 are valid QNS values.
@property ( nonatomic ) int8_t qns;

/// @brief Checks if the configuration in this object is valid.
/// Throws "IllegalConfigurationException" if the configuration is not valid.
- ( void ) validate;

@end
