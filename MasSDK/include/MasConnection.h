
#import <Foundation/Foundation.h>

/// @brief Contains all of the configuration information for an individual connection to MAS.
@interface MasConnection: NSObject<NSCoding>

/// @brief The IP address to connect to. Required.
@property ( nonatomic, strong ) NSString * ip;

/// @brief The port to connect to. Required.
@property ( nonatomic ) uint16_t port;

/// @brief Whether to use an encrypted connection to the server.
/// Default is false (encryption enabled).
@property ( nonatomic ) BOOL preferUnencrypted;

/// @brief Checks if the configuration in this object is valid.
/// Throws "IllegalConfigurationException" if the configuration is not valid.
- ( void ) validate;

/// @brief Create a new MasConnection instance.
/// @return A new MasConnection instance.
+ ( MasConnection * ) connection;

@end
