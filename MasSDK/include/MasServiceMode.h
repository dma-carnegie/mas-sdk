
#import <Foundation/Foundation.h>

/// @brief Specifies different MAS service modes.
typedef NS_ENUM ( uint8_t, MasServiceMode )
{
    /// @brief Uses virtual sockets and the virtual tunnel.
    MasServiceModeVirtual = 0,

    /// @brief Uses the iOS VPN API to enable whole-device aggregation, tunnelling all traffic through the MasService.
    MasServiceModeVpn,
};
