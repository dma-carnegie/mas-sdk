
#import <Foundation/Foundation.h>

/// @brief Contains all of the configuration information for the Analytics uploader.
@interface AnalyticsConfig: NSObject<NSCoding>

/// @brief The default upload timeout (in seconds). Equal to 5 minutes.
@property ( class, nonatomic, readonly ) int32_t DEFAULT_UPLOAD_TIMEOUT_SECONDS;

/// @brief The default upload period (in seconds). Equal to 1 hour.
@property ( class, nonatomic, readonly ) uint32_t DEFAULT_UPLOAD_PERIOD_SECONDS;

/// @brief The URL of the analytics server to which events should be uploaded.
///        Must not be empty.
@property ( nonatomic, strong ) NSString * analyticsServerUrl;

/// @brief The maximum time (in seconds) to block on an upload.
///        Optional; Defaults to DEFAULT_UPLOAD_TIMEOUT_SECONDS.
///        If zero or negative, uploads do not time out. Instead,
///        they block until completion or until an error occurs.
@property ( nonatomic ) int32_t uploadTimeoutSeconds;

/// @brief The time interval (in seconds) between uploading events.
///        Optional; Defaults to DEFAULT_UPLOAD_PERIOD_SECONDS.
@property ( nonatomic ) uint32_t uploadPeriodSeconds;

/// @brief Checks if the configuration in this object is valid.
/// Throws "IllegalConfigurationException" if the configuration is not valid.
- ( void ) validate;

/// @brief Creates a new AnalyticsConfig initialized with default values for all optional fields.
/// @param analyticsServerUrl The URL of the analytics server to which events should be uploaded.
/// @return A new AnalyticsConfig instance.
+ ( AnalyticsConfig * ) configWithAnalyticsServerUrl:( NSString * ) analyticsServerUrl;

@end
