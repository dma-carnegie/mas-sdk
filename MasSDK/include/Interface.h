
#import <Foundation/Foundation.h>
#import "InterfaceIndex.h"

/// @brief Convenience macro that evaluates to the Wi-Fi Interface instance.
#define InterfaceWiFi      [ Interface wifi ]

/// @brief Convenience macro that evaluates to the Mobile Interface instance.
#define InterfaceMobile    [ Interface mobile ]

/// @brief Represents a physical interface.
@interface Interface: NSObject<NSCopying, NSCoding>

/// @brief The underlying interface index.
@property ( readonly, nonatomic ) InterfaceIndex index;

/// @brief The human readable name of the interface: 'Wi-Fi' or 'Mobile'
@property ( readonly, nonatomic ) NSString * name;

/// @brief Same as name but without any symbols: 'WiFi' or 'Mobile'
@property ( readonly, nonatomic ) NSString * simpleName;

/// @return Get the Interface instance that represents the Wi-Fi interface.
/// @return The Interface instance that represents the Wi-Fi interface.
+ ( Interface * ) wifi;

/// @return Get the Interface instance that represents the Mobile interface.
/// @return The Interface instance that represents the Mobile interface.
+ ( Interface * ) mobile;

/// @brief Get the Interface instance that represents the given interface index.
/// @param [in] index The interface index to get an Interface instance for.
/// @return The Interface instance that represents the given interface index on success; nil on failure.
+ ( Interface * ) interfaceWithIndex:( InterfaceIndex ) index;

@end
