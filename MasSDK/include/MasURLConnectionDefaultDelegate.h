
#import <Foundation/Foundation.h>

/// @brief The default delegate provided to the MasURLConnection when none is specified for a request.
/// Buffers all data received. If a completion handler is provided, it will be invoked when the transfer
/// completes with any data received or errors generated.
@interface MasURLConnectionDefaultDelegate: NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>

/// @brief Initializes the delegate.
- ( instancetype ) init;

/// @brief Initializes the delegate with a block to be executed when the transfer finishes.
/// @param [in] queue The operation queue on which to execute the block.
/// @param [in] handler The block to execute when the transfer finishes.
- ( instancetype ) initWithQueue:( NSOperationQueue * ) queue
               completionHandler:( void ( ^ )( NSURLResponse * response, NSData * data,
                                    NSError * connectionError ) )handler;

/// @brief Gets the data received from the request.
/// @return The data from the transfer, if there was any; nil otherwise.
- ( NSData * ) data;

/// @brief Gets the URL response from the request.
/// @return The NSURLResponse from the request, if there was one; nil otherwise.
- ( NSURLResponse * ) response;

/// @brief Gets the error from the request.
/// @return The NSError from the request, if there was one; nil otherwise.
- ( NSError * ) error;

@end
