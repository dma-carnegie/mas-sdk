
#import <Foundation/Foundation.h>

/// @brief Contains statistics for VOIP traffic over the SDK.
@interface VoipStats: NSObject

/// @brief The round trip latency in milliseconds. -1 if unknown.
/// It is the average latency of MAS links used for delivering VOIP packets during the measurement period.
@property ( readonly, nonatomic ) int32_t latency;

/// @brief The jitter in millisecond. -1 if unknown.
/// Calculated based on timestamps in RTP packets.
@property ( readonly, nonatomic ) int32_t jitter;

/// @brief The packet loss of the RTP packets (percentage). -1 if unknown.
@property ( readonly, nonatomic ) int8_t packetLoss;

/// @brief VOIP MOS. -1 if unknown.
@property ( readonly, nonatomic ) double mos;

/// @brief The total duration of audio lost (ms) since a new RTP flow has been seen. -1 if unknown.
@property ( readonly, nonatomic ) int32_t audioLoss;

/// @brief The total number of VoIP packets received by the client.
/// It includes all copies of packets - so if a packet was sent in two copies (due to seamless scheduling),
/// it will increase the number of 'total' packets by 2.
@property ( readonly, nonatomic ) uint32_t totalPackets;

/// @brief The number of unique VoIP packets received by the client. It doesn't include the copies.
@property ( readonly, nonatomic ) uint32_t uniquePackets;

/// @brief The total number of VoIP bytes received by the client.
/// It includes all copies of packets - so if a packet was sent in two copies (due to seamless scheduling),
/// it will increase the number of 'total' bytes twice.
@property ( readonly, nonatomic ) uint64_t totalData;

/// @brief The number of unique VoIP bytes received by the client. It doesn't include the copies.
@property ( readonly, nonatomic ) uint64_t uniqueData;

@end
