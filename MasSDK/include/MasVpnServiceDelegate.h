
#import <Foundation/Foundation.h>

/// @brief The interface for receiving status updates from the MasVpnService.
///
/// Note: all delegate methods will be called from a single background thread, not the main UI thread.
@protocol MasVpnServiceDelegate<NSObject>

@optional

/// @brief Called when the VPN status has changed.
/// If the VPN is enabled, it means the MAS SDK is now tunnelling all traffic on the device.
///
/// @param [in] isVpnEnabled Whether the VPN is enabled or not.
- ( void ) masVpnStatusUpdated:( BOOL ) isVpnEnabled;

@end
