
#import "TrafficStatsDataPoint.h"

/// @brief The interface that exposes MasService's traffic stats.
@interface MasTrafficStats: NSObject

/// @brief Start monitoring traffic stats.
- ( void ) startMonitoring;

/// @brief Stop monitoring traffic stats.
- ( void ) stopMonitoring;

/// @brief Get the historical traffic stats between start and end.
/// @param [in] start The start of the time to get stats from.
/// @param [in] end The end of the time to get stats from.
/// @param [in] completionHandler The handler to receive the traffic history, which is in chronological order.
- ( void ) getTrafficStatsHistoryFrom:( NSDate * ) start
                                   to:( NSDate * ) end
                    completionHandler:( void ( ^ )( NSArray<TrafficStatsDataPoint *> * history ) )completionHandler;

/// @brief Clear all history up to a given time
/// @param [in] end The time to clear all history before.
- ( void ) clearHistoryUpTo:( NSDate * ) end;

@end
