
#import "MasStateDelegate.h"

/// @brief The interface that exposes MasService's state via properties and delegates.
@interface MasState: NSObject

/// @brief The current MasServiceState.
@property ( atomic, readonly ) MasServiceState masServiceState;

/// @brief The current MasServiceConnectivityState.
@property ( atomic, readonly ) MasServiceConnectivityState masServiceConnectivityState;

/// @brief Map of Interface to the MAS server IP address and port
/// (as an NSString with the format "<ip>:<port>") configured for that interface.
/// The MAS server for an interface will be nil if no MAS server is configured for it.
@property ( atomic, strong, readonly ) NSDictionary<Interface *, NSString *> * masServers;

/// @brief Contains Interfaces over which we have MAS server connections.
@property ( atomic, strong, readonly ) NSSet<Interface *> * masInterfacesConnected;

/// @brief Register a MasState delegate. This does nothing if the delegate is already registered.
/// @param [in] delegate The delegate to register.
- ( void ) registerDelegate:( id<MasStateDelegate> )delegate;

/// @brief Unregister a MasState delegate. This does nothing if no such delegate is registered.
/// @param [in] delegate The delegate to unregister.
- ( void ) unregisterDelegate:( id<MasStateDelegate> )delegate;

@end
