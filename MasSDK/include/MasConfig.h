
#import "MasConnection.h"
#import "MasSchedulerType.h"
#import "MasServiceInitFlags.h"
#import "MasSocketTag.h"
#import "Interface.h"
#import "QualityCriteria.h"
#import "SpillOverConfig.h"
#import "AnalyticsConfig.h"
#import "MasServiceMode.h"

/// @brief Contains all of the configuration information for the MAS SDK.
@interface MasConfig: NSObject<NSCoding>

/// @brief Map of Interface to the connection details of a MAS server configured for that interface.
/// Either this or masServerName must be set.
@property ( nonatomic, strong ) NSDictionary<Interface *, MasConnection *> * masConnections;

/// @brief Server name to use to connect to MAS.
/// This uses _mas.<proto>.<masServerName> DNS SRV records,
/// or if they don't exist, the A/AAAA records for <masServerName>.
/// Either this or masConnections must be set.
@property ( nonatomic, strong ) NSString * masServerName;

/// @brief Port number to use when using masServerName with DNS A/AAAA records instead of SRV records.
@property ( nonatomic ) uint16_t masDefaultPortNumber;

/// @brief Whether encryption is preferred when using masServerName.
/// Default is false (encryption enabled).
@property ( nonatomic ) BOOL preferUnencrypted;

/// @brief The max time in seconds we will spend trying to connect to a server before giving up and
/// trying to connect to a different server.
@property ( nonatomic ) uint16_t masServerConnectTimeout;

/// @brief Number of seconds to wait before trying to connect to a previously failed server again.
@property ( nonatomic ) uint16_t masServerFailDuration;

/// @brief URL to use to request client certificate. Required.
@property ( nonatomic, strong ) NSString * certificateUrl;

/// @brief List of ciphers to use for encrypting data. Optional.
/// Default is “HIGH:MEDIUM:!kDH:!kEDH:!kECDH:!kEECDH”
@property ( nonatomic, strong ) NSString * cipherList;

/// @brief Timeout for individual connection attempts to MAS (seconds).
/// Optional, only used if > 0, default is 10 seconds.
/// This must not be greater than masServerConnectTimeout.
@property ( nonatomic ) uint16_t connectTimeout;

/// @brief Delay between individual connection retries to MAS (seconds).
/// Optional, only used if > 0, default is 10 seconds.
@property ( nonatomic ) uint16_t restartDelay;

/// @brief Default scheduler for sockets created with the ACE+ SDK. Optional.
/// Default is MasSchedulerTypeSeamless.
@property ( nonatomic ) MasSchedulerType defaultScheduler;

/// @brief The minimum acceptable quality threshold for the threshold seamless scheduler.
/// This is only used by the threshold seamless scheduler.
///
/// When replication ends, the threshold seamless scheduler will select the highest priority
/// link that satisfies this threshold.
///
/// If nil, the threshold seamless scheduler will fallback to priority-based link selection.
///
/// The default quality threshold for the scheduler is VOIP MOS >= 3.85
@property ( nonatomic, strong ) QualityCriteria * thresholdSeamlessQualityThreshold;

/// @brief The traffic spill-over configuration for single-stream aggregation.
/// This is only used by single-stream aggregation virtual sockets.
/// The default configuration aggregates over all interfaces without limits.
@property ( nonatomic, strong ) SpillOverConfig * spillOverConfig;

/// @brief The max time in seconds we allow MasService to stay in a state without MAS connectivity
/// while we have internet connectivity. If we're still not connected to MAS after this time period
/// expires, we will disable MasService's features. However MasService will still try to connect in
/// the background, and it will be re-enabled once connected.
@property ( nonatomic ) uint16_t masConnectivityGracePeriod;

/// @brief The analytics configuration. If nil, then analytics won't be collected.
@property ( nonatomic, strong ) AnalyticsConfig * analyticsConfig;

/// @brief Configures the mode in which the service should run.
/// This CANNOT be changed after starting the service.
/// Default is MasServiceModeVirtual.
@property ( nonatomic ) MasServiceMode masServiceMode;

/// @brief The bundle ID of the PacketTunnelProvider class which inherits from our MasPacketTunnelProvider.
/// This typically has the format "<app-package-name>.PacketTunnelProvider".
/// This is required for VPN mode to implement the VPN tunnel.
@property ( nonatomic, strong ) NSString * packetTunnelProviderBundleId;

/// @brief The iOS application group ID shared between the main app and the packet tunnel provider.
/// This is required for VPN mode so the main app can communicate with the provider.
@property ( nonatomic, strong ) NSString * appGroupId;

/// @brief The MAS socket tag that will be assigned to sockets created by the internal DNS (VDNS) resolver.
/// This is used by the MAS HTTP classes. This is optional, and defaults to MasSocketTagDefault. See MasSocketTags.h
@property ( nonatomic ) MasSocketTag dnsSocketTag;

/// @brief The option flags to use while initializing MAS virtual socket support. See MasServiceInitFlags.h
///
/// Note: this option cannot be changed after MasService is started and stopped, except by restarting the application.
@property ( nonatomic ) int initFlags;

/// @brief Checks if the configuration in this object is valid.
/// Throws "IllegalConfigurationException" if the configuration is not valid.
- ( void ) validate;

/// @brief Create a new MasConfig initialized with default values for all optional fields.
/// @return A new MasConfig instance.
+ ( MasConfig * ) config;

@end
