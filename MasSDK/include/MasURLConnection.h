
#import <Foundation/NSURLConnection.h>

/// @brief A class for making HTTP / HTTPS requests over MAS using virtual tunnel sockets.
/// Supports all of the functionality of the NSURLConnection, with the following exceptions:
///   NewsStand Kit and the associated NSURLConnectionDownloadDelegate
///   Authentication
///   Caching
///   HTTP Body Streams
///   Cookie handling
///   HTTP Pipelining
///   NetworkServiceType handling
@interface MasURLConnection: NSURLConnection

@end
