
#import "MasVpnServiceStatusCode.h"
#import "MasVpnConfig.h"
#import "MasVpnServiceDelegate.h"

/// @brief Error domain for NSErrors from MasVpnService.
/// This means the error code is one of MasVpnServiceStatusCode.
extern NSErrorDomain const MasVpnServiceStatusCodeDomain;

/// @brief The interface for controlling the MAS SDK in VPN mode.
/// Instances of this class cannot be created outside of MasService.
/// Instead it should be accessed via [ MasService sharedService ].masVpnInterface
/// when MasService is started in VPN mode.
@interface MasVpnService: NSObject

/// @brief Request for VPN permission.
/// This is provided as a convenience method to request permission before attempting to enable the VPN.
/// Calling either of the enableVpn methods will request permission regardless, if not already granted.
///
/// If everything is configured correctly in the project, calling this method will bring up
/// the iOS system prompt for saving a new VPN configuration for the current app.
///
/// See provided documentation for full step-by-step details.
///
/// @param [in] completionHandler The block function that is called when the VPN permission request succeeds or fails.
///                               On success, nil is passed to the function.
///                               Otherwise the failure reason is passed.
- ( void ) requestVpnPermission:( void ( ^ )( NSError * error ) )completionHandler;

/// @brief Enables the VPN, if it is not already enabled.
/// When the VPN is enabled, MasService is tunnelling all traffic on the device.
/// Device traffic is unaffected while the VPN is not enabled.
///
/// Calling this function also clears any previously saved VPN configuration.
/// Use enableVpnWithConfig: if a specific VPN configuration is needed.
///
/// This does nothing if the VPN is already enabled.
///
/// @param [in] completionHandler The block function that is called when enabling the VPN succeeds or fails.
///                               On success, MasVpnServiceStatusCodeSuccess is passed to the function.
///                               Otherwise the failure reason is passed.
- ( void ) enableVpn:( void ( ^ )( MasVpnServiceStatusCode status ) )completionHandler;

/// @brief Enables the VPN with a specific configuration.
/// This may not enable the VPN immediately, depending on the config.
/// See MasVpnConfig for more details.
///
/// This does nothing if the VPN is already enabled.
///
/// @param [in] vpnConfig Contains everything needed for running and controlling the VPN.
///                       If vpnConfig is nil, this behaves the same as enableVpn:.
/// @param [in] completionHandler The block function that is called when enabling the VPN succeeds or fails.
///                               On success, MasVpnServiceStatusCodeSuccess is passed to the function.
///                               Otherwise the failure reason is passed.
- ( void ) enableVpnWithConfig:( MasVpnConfig * ) vpnConfig
             completionHandler:( void ( ^ )( MasVpnServiceStatusCode status ) )completionHandler;

/// @brief Disables the VPN, if it is enabled.
///
/// MasService will not tunnel any traffic until the VPN is enabled again.
- ( void ) disableVpn;

/// @brief Register a MasVpnServiceDelegate delegate.
/// If this is a new delegate, it will always be called with the current VPN status after a slight delay.
/// If the delegate is already registered, no special callbacks will happen.
///
/// @param [in] delegate The delegate to register.
- ( void ) registerDelegate:( id<MasVpnServiceDelegate> )delegate;

/// @brief Unregister a MasVpnServiceDelegate delegate.
/// This does nothing if no such delegate is registered.
/// @param [in] delegate The delegate to unregister.
- ( void ) unregisterDelegate:( id<MasVpnServiceDelegate> )delegate;

@end
