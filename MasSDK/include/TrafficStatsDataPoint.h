
#import "InterfaceStats.h"

/// @brief Contains statistics for traffic over all interfaces at a given point in time.
@interface TrafficStatsDataPoint: NSObject<NSCoding>

/// @brief The data for all the interfaces.
@property ( readonly, nonatomic, strong ) NSDictionary<Interface *, InterfaceStats *> * interfaceData;

/// @brief The time the data point was recorded.
@property ( readonly, nonatomic ) NSDate * time;

@end
