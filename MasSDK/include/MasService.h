
#import "MasConfig.h"
#import "MasState.h"
#import "MasQuality.h"
#import "MasVpnService.h"
#import "MasTrafficStats.h"

/// @brief Application-facing MAS SDK service class.
/// Contains everything needed for starting the MasService.
@interface MasService: NSObject

/// @brief Get the global MasService instance.
/// @return The global MasService instance.
+ ( MasService * ) sharedService;

/// @brief Registers an application with the MasService.
///
/// Required for initializing essential items in the MasService specific to the application.
/// This should be called at the beginning of the application. It is safe to register multiple times if you are unsure
/// whether or not the application is registered.
+ ( void ) registerApplication;

/// @brief Configures the MAS SDK.
///
/// Must be called before 'start'.
/// Any calls to 'setup' after calling 'start' will only apply to the next 'start',
/// and only the last call to 'setup' will apply.
///
/// @param [in] config Contains all of the configuration information for the MAS SDK.
- ( void ) setupWithConfig:( MasConfig * ) config;

/// @brief Configures the MasService instance and starts it.
///
/// This is the same as calling setupWithConfig then start. See start.
///
/// Throws "IllegalConfigurationException" if MasConfig is not valid.
/// Throws "IllegalStateException" if MasService is already started, or has been started in the past,
///                                or if licensing resources are invalid.
/// Note: we do not support starting MasService more than once!
///
/// @param [in] config Contains all of the configuration information for the MAS SDK
- ( void ) startWithConfig:( MasConfig * ) config;

/// @brief Starts the threads and services associated with the MAS SDK.
///
/// Must be called before any virtual sockets are created by the MAS HTTP
/// classes or the Native Library. This should typically be run at the beginning
/// of the application (no connections are made until a socket is created).
///
/// Throws "IllegalConfigurationException" if MasConfig is not valid.
/// Throws "IllegalStateException" if MasService is already started, or has been started in the past,
///                                or if licensing resources are invalid.
/// Note: we do not support starting MasService more than once!
- ( void ) start;

/// @brief Stops the threads and services associated with the MAS SDK.
///
/// All current connections/sockets are terminated.
/// This should typically be done at the end of the application.
///
/// This may not actually stop the service immediately!
- ( void ) stop;

/// @brief Exposes whether or not MasService is started.
/// True if MasService is started; False otherwise.
- ( BOOL ) isStarted;

/// @brief Get the interface for retrieving MasService state.
/// @return The interface for retrieving MasService state.
- ( MasState * ) masState;

/// @brief Get the interface for retrieving MasService connection quality.
/// @return The interface for retrieving MasService connection quality.
- ( MasQuality * ) masQuality;

/// @brief Get the interface for retrieving MasService traffic stats.
/// @return The interface for retrieving MasService traffic stats.
- ( MasTrafficStats * ) masTrafficStats;

/// @brief Get the interface for controlling MasService in VPN mode.
/// @return The interface for controlling MasService in VPN mode.
/// This returns nil if MasService is not started in VPN mode.
- ( MasVpnService * ) masVpnService;

/// @brief Get the current configuration of the MAS SDK.
/// @return The current configuration of the MAS SDK.
- ( MasConfig * ) masConfig;

/// @brief Sets the default MAS scheduler to be used.
/// @param [in] schedulerType The MAS scheduler type.
- ( void ) setDefaultScheduler:( MasSchedulerType ) schedulerType;

/// @brief Set the traffic spill-over configuration for single-stream aggregation.
/// This will apply to any existing single-stream aggregation virtual sockets and new ones.
/// @param [in] spillOverConfig The traffic spill-over configuration for single-stream aggregation.
- ( void ) setSpillOverConfig:( SpillOverConfig * ) spillOverConfig;

/// @brief Sets the subscriber ID to be used for licensing checkins.
///
/// This function may be called any number of times, with the last non-nil
/// value being used. The new value will take effect at the next check-in
/// with the licensing server.
///
/// @param context The context from which this function is being called.
/// @param subscriberId The subscriber ID to use for licensing checkins.
///                     Should be at most 64 characters. If longer, only the first 64 are used.
///                     This should be a value that is meaningful to the customer
///                     (e.g. a customer ID or an account ID), but it should not
///                     contain any personally identifying information (e.g. an
///                     email address or a phone number) that could present a
///                     risk to their privacy.
- ( void ) setSubscriberId:( NSString * ) subscriberId;

/// @brief Sets the environment to be used for licensing checkins.
///
/// This function may be called any number of times, with the last non-nil
/// value being used. The new value will take effect at the next check-in
/// with the licensing server.
///
/// @param context The context from which this function is being called.
/// @param environment The environment to use for licensing checkins.
///                    Should be at most 32 characters. If longer, only the first 32 are used.
- ( void ) setEnvironment:( NSString * ) environment;

@end
