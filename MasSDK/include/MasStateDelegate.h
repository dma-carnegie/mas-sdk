
#import "MasServiceState.h"
#import "MasServiceConnectivityState.h"
#import "Interface.h"

/// @brief The interface for receiving updates when MasService's state changes.
///
/// Note: all delegate methods will be called from a single background thread, not the main UI thread.
@protocol MasStateDelegate<NSObject>

@optional

/// @brief Called when the MasServiceState has changed.
/// @param [in] masServiceState The new MasServiceState.
- ( void ) masServiceStateUpdated:( MasServiceState ) masServiceState;

/// @brief Called when the MasServiceConnectivityState has changed.
/// @param [in] masServiceConnectivityState The new MasServiceConnectivityState.
- ( void ) masServiceConnectivityStateUpdated:( MasServiceConnectivityState ) masServiceConnectivityState;

/// @brief Called when the MAS server configured for an interface has changed.
/// @param [in] masServer The new MAS server configured for the interface.
/// @param [in] interface The interface whose MAS server has changed.
- ( void ) masServerUpdated:( NSString * ) masServer forInterface:( Interface * ) interface;

/// @brief Called when the state of a MAS server connection over an interface has changed.
/// @param [in] interface The interface whose MAS server connection state has changed.
/// @param [in] isConnected Whether we are connected to the MAS server over the interface.
- ( void ) masInterface:( Interface * ) interface isConnected: ( BOOL ) isConnected;

@end
