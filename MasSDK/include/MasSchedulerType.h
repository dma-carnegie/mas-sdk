
#import <Foundation/Foundation.h>

/// @brief Specifies different scheduler types.
typedef NS_ENUM ( uint8_t, MasSchedulerType )
{
    /// @brief Scheduler that uses single best interface and seamlessly moves the traffic to other interfaces if needed.
    MasSchedulerTypeSeamless,

    /// @brief Scheduler that replicates traffic and sends it over all available interfaces.
    MasSchedulerTypeReplication,

    /// @brief Scheduler that distributes traffic across multiple interfaces.
    MasSchedulerTypeBandwidthAggregation,

    /// @brief Scheduler that will send traffic over WiFi if it is available, using mobile interface otherwise.
    MasSchedulerTypePreferWiFi,

    /// @brief Scheduler that will send traffic over mobile if it is available, using WiFi interface otherwise.
    MasSchedulerTypePreferMobile,

    /// @brief Scheduler that will only send traffic over WiFi.
    MasSchedulerTypeWiFiOnly,

    /// @brief Scheduler that will only send traffic over mobile.
    MasSchedulerTypeMobileOnly,

    /// @brief Same as Seamless, except it that selects the highest priority network above a minimum quality threshold.
    MasSchedulerTypeThresholdSeamless,

    /// @brief Scheduler that distributes traffic across multiple interfaces,
    /// and also tries to balance traffic between them.
    MasSchedulerTypeBandwidthAggregationBalanced,

    /// @brief Scheduler that performs aggregation for individual streams, i.e. packet based aggregation.
    MasSchedulerTypeSingleStreamAggregation,
};
