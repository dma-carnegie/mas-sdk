
#import <Foundation/Foundation.h>

/// @brief An enum of possible MAS seamless scheduler events.
typedef NS_ENUM ( uint8_t, MasSeamlessEvent )
{
    // Unknown event
    MasSeamlessEventUnknown = 0,

    // Replication started due to poor quality of the current network.
    MasSeamlessEventBadQuality = 1,

    // Replication started due to worsening quality of the current network.
    MasSeamlessEventBadQualityTrend = 2,

    // Replication started due to improving quality of a network with higher priority than the current one.
    MasSeamlessEventGoodQualityTrend = 3,

    // Replication state changed because a network has been lost.
    MasSeamlessEventNetworkLoss = 4,

    // Replication state changed because a new network became available.
    MasSeamlessEventNewNetwork = 5,

    // Replication started to re-assess available networks.
    MasSeamlessEventNetworkTest = 6,

    // Replication started because none of the available networks was considered good.
    MasSeamlessEventNoGoodNetwork = 7,

    // Replication ended.
    MasSeamlessEventReplicationEnded = 8,

    // Replication started due to the current network being slower than expected.
    MasSeamlessEventNetworkSlow = 9,
};
