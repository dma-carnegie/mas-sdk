
#import <Foundation/NSURLProtocol.h>

/// @brief A class that allows configuring NSURLSession to perform HTTP / HTTPS requests
/// over MAS using virtual tunnel sockets.
///
/// This can be used as follows for a new NSURLSession instance:
///
///   NSURLSessionConfiguration * config = [ NSURLSessionConfiguration defaultSessionConfiguration ];
///   config.protocolClasses = @[ [ MasURLProtocol class ] ];
///
///   NSURLSession * session = [ NSURLSession sessionWithConfiguration:config ];
///
/// Or for the default [ NSURLSession sharedSession ]:
///
///   [ NSURLProtocol registerClass:[ MasURLProtocol class ] ];
///
///
/// Note: NSURLProtocolClient doesn't provide a way for custom protocols, such as this one, to report upload progress
/// for a POST or PUT request. This has two consequences:
///
///  - For any long-running PUT or POST request using MasURLProtocol, the request's timeoutInterval must be set to a
///    large enough value, or -1.0 to signify no timeout, otherwise NSURLSession will prematurely timeout the request.
///    Only when a large enough timeout value (or -1.0) is set, will the upload request complete correctly.
///
///  - For any PUT or POST request, the didSendBodyData: delegate method in NSURLSessionTaskDelegate will not be
///    called by NSURLSession to report upload progress. In this case we provide a workaround via MasURLProtocol's
///    registerTask:withSession: which will register a task so that we manually call the delegate method.
///    See registerTask:withSession: below.
///
@interface MasURLProtocol: NSURLProtocol

/// @brief This should be called whenever a new NSURLSessionTask is created by a NSURLSession that uses MasURLProtocol.
/// This is needed so that the didSendBodyData: delegate method in NSURLSessionTaskDelegate will be called for any
/// tasks created using this protocol. Otherwise anything that requires didSendBodyData: may not work correctly.
///
/// For example using NSURLSession:
///   NSURLSession * session;
///   ...
///   NSURLSessionTask * task = [ session uploadTaskWithRequest:request fromFile:file ];
///   [ MasURLProtocol registerTask:task withSession:session ];
///
/// @param [in] task The NSURLSessionTask instance created by the session.
/// @param [in] session The NSURLSession instance that uses MasURLProtocol.
+ ( void ) registerTask:( NSURLSessionTask * ) task withSession:( NSURLSession * ) session;

@end
