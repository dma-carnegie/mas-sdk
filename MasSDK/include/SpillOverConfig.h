
#import "Interface.h"

/// @brief Represents the traffic spill-over configuration for single-stream aggregation.
@interface SpillOverConfig: NSObject<NSCoding>

/// @brief The single-stream aggregation scheduler will add interfaces in the order defined
/// by this list until the target throughput is achieved.
///
/// The first available interface on this list is always treated as unlimited.
/// This means that the interface will not be rate-limited even if the total throughput is greater than target.
///
/// This list must be non-empty.
@property ( nonatomic, strong ) NSArray<Interface *> * interfacePriorities;

/// @brief The target throughput value for the single-stream aggregation scheduler in bytes per second.
/// The scheduler will add interfaces in priority order until the target throughput is achieved.
/// If set to 0, the scheduler will only use the first available interface without limits.
/// If set to a negative value, the scheduler will aggregate over all interfaces without limits.
@property ( nonatomic ) int32_t targetThroughput;

/// @brief Checks if the configuration in this object is valid.
/// Throws "IllegalConfigurationException" if the configuration is not valid.
- ( void ) validate;

@end
