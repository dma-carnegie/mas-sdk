
#import <Foundation/Foundation.h>

/// @brief An enum of possible states of the MasService.
typedef NS_ENUM ( uint8_t, MasServiceState )
{
    /// @brief MasService is not ready to use.
    MasServiceStateNotReady = 0,

    /// @brief MasService has limited capabilities.
    ///        Sockets can be created but not connected.
    MasServiceStateLimited,

    /// @brief MasService is ready to use.
    ///        Sockets can be created and connected.
    MasServiceStateReady
};
