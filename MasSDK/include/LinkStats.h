
#import "Interface.h"

/// @brief Contains statistics for a link to a MAS server.
@interface LinkStats: NSObject

/// @brief The physical interface of the link.
@property ( readonly, nonatomic, strong ) Interface * interface;

/// @brief The link's round trip latency in milliseconds. -1 if unknown.
@property ( readonly, nonatomic ) int32_t latency;

/// @brief The link's jitter in milliseconds (std. dev. of round trip times). -1 if unknown.
@property ( readonly, nonatomic ) int32_t jitter;

/// @brief The link's packet loss as a percentage. -1 if unknown.
@property ( readonly, nonatomic ) int8_t packetLoss;

/// @brief The link's Mean Opinion Score for web traffic. -1 if unknown.
@property ( readonly, nonatomic ) float mosWeb;

/// @brief The link's Mean Opinion Score for VOIP traffic. -1 if unknown.
@property ( readonly, nonatomic ) float mosVoip;

/// @brief The link's Mean Opinion Score for video traffic. -1 if unknown.
@property ( readonly, nonatomic ) float mosVideo;

/// @brief The link's QNS value (0-100). -1 if unknown.
@property ( readonly, nonatomic ) int8_t qns;

@end
