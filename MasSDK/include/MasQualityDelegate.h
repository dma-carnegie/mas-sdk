
#import "MasSeamlessState.h"
#import "LinkStats.h"
#import "InterfaceStats.h"
#import "VoipStats.h"

/// @brief The interface for receiving updates when MasService's connection quality changes.
///
/// Note: all delegate methods will be called from a single background thread, not the main UI thread.
@protocol MasQualityDelegate<NSObject>

@optional

/// @brief Called when the seamless scheduler state has changed.
/// @param [in] masSeamless The new state of the seamless scheduler.
///                         This is nil if we don't have any information about it.
- ( void ) masSeamlessStateUpdated:( MasSeamlessState * ) masSeamlessState;

/// @brief Called when the link stats of a MAS connection has changed.
/// @param [in] linkStats The new link stats of a MAS connection.
- ( void ) masLinkStatsUpdated:( LinkStats * ) linkStats;

/// @brief Called when the interface stats of a MAS connection has changed.
/// @param [in] interfaceStats The new interface stats of a MAS connection.
- ( void ) masInterfaceStatsUpdated:( InterfaceStats * ) interfaceStats;

/// @brief Called when the link stats and interface stats for all MAS connections have been cleared.
- ( void ) masAllStatsCleared;

/// @brief Called when the VOIP stats of an active VOIP call has changed.
/// @param [in] voipStats The new VOIP stats of the active VOIP call.
///                       This is nil if we don't have any information about it, or there is no active call.
- ( void ) masVoipStatsUpdated:( VoipStats * ) voipStats;

@end
