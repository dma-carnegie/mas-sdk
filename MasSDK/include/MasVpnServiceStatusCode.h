
#import <Foundation/Foundation.h>

/// @brief Contains possible status codes when using the MAS VPN API.
typedef NS_ENUM ( uint8_t, MasVpnServiceStatusCode )
{
    /// @brief Operation completed successfully.
    MasVpnServiceStatusCodeSuccess = 0,

    /// @brief The MasService does not have permission to operate the VPN.
    MasVpnServiceStatusCodeNoPermission,

    /// @brief An internal error prevented the VPN from being enabled.
    MasVpnServiceStatusCodeInternalError,

    /// @brief The MasService is not in VPN mode; the VPN cannot be enabled.
    MasVpnServiceStatusCodeNotInVpnMode,

    /// @brief The VPN cannot be initialized because NETunnelProviderManager is not initialized.
    MasVpnServiceStatusCodeManagerNotInitialized,
};
