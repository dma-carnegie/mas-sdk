
#import <NetworkExtension/NetworkExtension.h>

#import "MasPacketTunnelProviderError.h"

/// @brief Error domain for NSErrors from MasPacketTunnelProvider.
/// This means the error code is one of MasPacketTunnelProviderError.
extern NSErrorDomain const MasPacketTunnelProviderErrorDomain;

/// @brief MasService backed implementation of NEPacketTunnelProvider, for iOS VPN mode.
///
/// To use this class, follow the normal steps to add a packet tunnel provider:
///  - add NetworkExtension provisioning profile, and relevant entitlements and capabilities
///  - add a packet tunnel application extension, with relevant entitlements
///  - add a NEPacketTunnelProvider subclass, and make sure your NSExtensionPrincipalClass points to it
///
/// Then for your NEPacketTunnelProvider subclass, simply inherit from MasPacketTunnelProvider instead.
/// And you can use the MasVpnService interface to configure/enable/disable the VPN.
///
/// See provided documentation for full step-by-step details.
@interface MasPacketTunnelProvider: NEPacketTunnelProvider

@end
