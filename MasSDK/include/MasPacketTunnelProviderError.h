
#import <Foundation/Foundation.h>

/// @brief An enum of possible errors when using the MasPacketTunnelProvider class.
typedef NS_ENUM ( uint8_t, MasPacketTunnelProviderError )
{
    /// @brief The protocol config received by the provider is invalid.
    MasPacketTunnelProviderErrorInvalidProtocolConfig,

    /// @brief MasConfig received by the provider is invalid.
    MasPacketTunnelProviderErrorInvalidMasConfig,

    /// @brief MasVpnConfig received by the provider is invalid.
    MasPacketTunnelProviderErrorInvalidVpnConfig,

    /// @brief MasClient failed to initialize.
    MasPacketTunnelProviderErrorInitializationFailed,
};
